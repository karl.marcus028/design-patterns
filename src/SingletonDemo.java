/**
 * This example shows how the singleton pattern enforces only a single instance of
 * a class to ever get produced and exist throughout an application's lifetime.
 *
 * due keep in mind that this solution can have troubles with multithreading, but as long
 * as it is one-thread it won't be an issue.
 */

class AirForceOne {

    //The sole instance of the class
    private static AirForceOne onlyInstance;

    //Make the constructor private so it's only accessible to members of the class
    private AirForceOne() {

    }

    public void fly(){
        System.out.println("Airforce on is flying...");
    }

    //create a static method for object creation
    public static AirForceOne getInstance(){

        //Only instantiate the object when needed.
        if (onlyInstance == null){
            onlyInstance = new AirForceOne();
        }

        return onlyInstance;
    }
}

public class SingletonDemo {
    public static void main(String[] args) {
        AirForceOne airforceOne = AirForceOne.getInstance();
        airforceOne.fly();
    }
}
